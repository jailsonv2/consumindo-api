var url = 'http://localhost:3000';


async function getContent() {
  try {
    var promessa = await fetch(url);
    var { result } = await promessa.json();

    console.log(result);
    show(result);

  } catch (error) {
    console.error(error + 'jailson');
  } 
}

getContent();

function show(result) {
  let output = `<li>${result}</li>`;

  document.querySelector('main').innerHTML = output;
}
