const cors = require('cors');
const express = require('express');
const app = express();
const axios = require('axios');

app.use(cors());

app.get('/', async (_, res) => {
  // response é a resposta do axios
  const { data }  = await axios('https://api.adviceslip.com/advice');
  const result = await data.slip.advice;

  return res.json({ result });
});

app.listen('3000');
